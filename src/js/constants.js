const ERRORS = {
  NOT_COURSE_FOLDER: 'notCourseFolder',
  USERJS_NOT_FOUND: 'userJsNotFound',
  CANNOT_SAVE_ZIP: 'cannotSaveZIP',
  CANNOT_EXPORT: 'cannotExport',
  CANNOT_IMPORT: 'cannotImport',
};

export {
  ERRORS,
};
