import fs from 'fs';
import AdmZip from 'adm-zip';

import { ERRORS } from './constants';

const { dialog } = require('electron').remote;

const checkFolderExisting = (tab) => {
  fs.access(`${tab.coursePath}/story.html`, (err) => {
    if (err?.code === 'ENOENT') {
      return new Error();
    }
    return true;
  });
};

const clearOldZip = (tab) => {
  fs.readdirSync(tab.coursePath).forEach((fileName) => {
    if (fileName.includes('.zip')) {
      fs.unlinkSync(`${tab.coursePath}/${fileName}`);
    }
  });
};

const createZip = (tab) => {
  const zip = new AdmZip();
  zip.addLocalFolder(`${tab.coursePath}`);
  zip.writeZip(`${tab.coursePath}/${tab.archiveName}.zip`);
};

const updateUserJs = (tab) => {
  const sourceUserRawdata = fs.readFileSync(tab.userPath, 'utf8', () => {});
  const targetUserRawdata = fs.readFileSync(`${tab.coursePath}/story_content/user.js`, 'utf8', () => {});

  const executeScriptIndex = targetUserRawdata.indexOf('function ExecuteScript');
  const initialTargetUserRawdata = targetUserRawdata.slice(executeScriptIndex);

  return fs.writeFileSync(`${tab.coursePath}/story_content/user.js`,
    `${sourceUserRawdata}\n\n${initialTargetUserRawdata}`,
    () => {});
};

const execute = (tab) => {
  try {
    checkFolderExisting(tab);
  } catch {
    return ERRORS.NOT_COURSE_FOLDER;
  }

  clearOldZip(tab);

  try {
    updateUserJs(tab);
  } catch {
    return ERRORS.USERJS_NOT_FOUND;
  }

  try {
    createZip(tab);
  } catch {
    return ERRORS.CANNOT_SAVE_ZIP;
  }

  return false;
};

const importConfig = async () => {
  const dialogData = await dialog.showOpenDialog({
    title: 'Select file',
    properties: ['openFile'],
  });

  if (!dialogData?.filePaths?.length) {
    return false;
  }

  const filePath = dialogData.filePaths[0];
  const rawdata = fs.readFileSync(filePath, () => {});
  const { name, ...rest } = JSON.parse(rawdata);

  return { name, ...rest };
};

const exportConfig = async (tab) => {
  const dialogData = await dialog.showSaveDialog({
    title: 'Save to',
    defaultPath: `${tab.label}-config.json`,
  });

  if (!dialogData?.filePath) {
    return false;
  }

  const fileForSave = JSON.stringify(tab, null, 2);

  return fs.writeFileSync(dialogData.filePath, fileForSave, () => {});
};

export {
  execute,
  exportConfig,
  importConfig,
};
