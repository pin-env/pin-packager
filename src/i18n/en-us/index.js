export default {
  emptyProject: 'Empty Project',
  fields: {
    projectName: {
      label: 'Project (configuration) name',
      placeholder: '',
    },
    coursePath: {
      label: 'Course path',
      placeholder: '',
    },
    userPath: {
      label: 'Custom user.js file path',
      placeholder: '',
    },
    audioPath: {
      label: 'Audiofiles path',
      placeholder: '',
    },
    archiveName: {
      label: 'Archive name',
      placeholder: '',
    },
  },
  buttons: {
    pack: 'Create course package',
    import: 'Import config',
    export: 'Export config',
    ok: 'OK',
  },
  audioComment: `Enable background music for your course. Specify the path to the folder with audiofiles.
    {pin} will be added to index.html`,
  pin: 'PIN-audioplayer',
  errors: {
    notCourseFolder: {
      header: 'Incorrect course directory',
      text: 'Selected directory has no course files',
    },
    userJsNotFound: {
      header: 'File user.js not found',
      text: 'Check user.js existing in choosen directory and target course folder',
    },
    cannotSaveZIP: {
      header: 'Failed to create ZIP-archive',
      text: 'Unknown problem during archivation process',
    },
    cannotExport: {
      header: 'Failed to export configuration file',
      text: 'Insufficient rights to create file',
    },
    cannotImport: {
      header: 'Failed to import configuration file',
      text: 'Selected file has incorrect extension or content problem',
    },
  },
  jsFileFormat: 'Executable .js files',
};
