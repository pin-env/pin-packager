export default {
  emptyProject: 'Новый Проект',
  fields: {
    projectName: {
      label: 'Название проекта (конфигурации)',
      placeholder: '',
    },
    coursePath: {
      label: 'Путь к папке курса',
      placeholder: '',
    },
    userPath: {
      label: 'Путь к кастомному файлу user.js',
      placeholder: '',
    },
    audioPath: {
      label: 'Путь к папке с аудиофайлами',
      placeholder: '',
    },
    archiveName: {
      label: 'Название архива',
      placeholder: '',
    },
  },
  buttons: {
    pack: 'Упаковать курс',
    import: 'Импортировать',
    export: 'Экспортировать',
    ok: 'ОК',
  },
  audioComment: 'Включите поддержку фонового аудио в курсе, указав путь к папке с аудиофайлами. В файл index.js будет добавлен {pin}',
  pin: 'PIN-аудиоплеер',
  errors: {
    notCourseFolder: {
      header: 'Некорректная директория курса',
      text: 'Не найдены файлы курса в указанной директории',
    },
    userJsNotFound: {
      header: 'Не найден файл user.js',
      text: 'Проверьте наличие файлов user.js в указанной директории и целевой папке курса',
    },
    cannotSaveZIP: {
      header: 'Не удалось создать ZIP-архив',
      text: 'Неизвестная проблема при создании архива',
    },
    cannotExport: {
      header: 'Не удалось экспортировать файл конфигурации',
      text: 'Недостаточно прав для создания файла',
    },
    cannotImport: {
      header: 'Не удалось импортировать файл конфигурации',
      text: 'Файл имеет неверный формат или содержимое',
    },
  },
  jsFileFormat: 'Исполняемые файлы .js',
};
